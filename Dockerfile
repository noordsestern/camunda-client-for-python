FROM adoptopenjdk:jdk

RUN apt-get update && apt-get install software-properties-common -y
RUN apt-get install python3 python3-pip wget -y
RUN pip3 install robotframework markdown Pygments pyyaml setuptools wheel twine