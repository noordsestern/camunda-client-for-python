# About

This is a generic client for Camunda generated from camunda's openapi specification. Therefore its source code is not provisioned. You can only install it with 

```
pip install generic-camunda-client
```

## Open API specs

Specification for generated python clients is retrieved from jars from Maven Central: https://search.maven.org/artifact/org.camunda.bpm/camunda-engine-rest-openapi
Just download a JAR matching your Camunda version, extract it and find the `openapi.json` inside.

The generator used is [OpenApi Generator 4.3.1](https://github.com/OpenAPITools/openapi-generator/releases/tag/v4.3.1) . I did not upgrade the generator, yet, for avoiding compatibility issues.