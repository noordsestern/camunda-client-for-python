import os
import yaml
import io

branch_version = os.environ.get('CI_PIPELINE_IID', '0.0.1')
package_version = os.environ.get('CI_COMMIT_TAG', branch_version)

openapi_config = dict(additionalProperties={
    # Specifies that only a library source code is to be generated.
    # Default: false
    'generateSourceCodeOnly': os.environ.get('GENERATE_SOURCECODE_ONLY', 'false'),

    # Hides the generation timestamp when files are generated.
    # Default: true
    'hideGenerationTimestamp': 'false',

    # library template (sub-template) to use: asyncio, tornado, urllib3
    # Default: urllib3
    'library': os.environ.get('PYTHON_CLIENT_FRAMEWORK', 'urllib3'),

    # python package name (convention: snake_case).
    # Default: openapi_client
    'packageName': 'generic_camunda_client',

    # python package URL.
    # Default: null
    'packageUrl': f'https://noordsestern.gitlab.io/camunda-client-for-python/{os.environ.get("CI_COMMIT_REF_SLUG")}/README.html',

    # python package version.
    # Default: 1.0.0
    'packageVersion': package_version,

    # python project name in setup.py (e.g. petstore-api).
    # Default: null
    'projectName': 'generic-camunda-client',

    # Set the recursion limit. If not set, use the system default value.
    # Default: null
    'recursionLimit': 'null',

    # Sort method arguments to place required parameters before optional parameters.
    # Default: true
    'sortParamsByRequiredFlag': 'true',

    # use the nose test framework
    # Default: false
    'useNose': 'false',
})

stream = io.open('document.yaml', 'w')
yaml.dump(openapi_config, stream)
