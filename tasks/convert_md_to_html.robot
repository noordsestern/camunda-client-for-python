*** Settings ***
Library    ../libraries/MarkdownLibrary.py

*** Variables ***
${INPUT_DIR}    output/docs
${RESULT_DIR}    test_output

*** Tasks ***
Convert all Markdown files to HTML
    ${all_htmls}    convert all markdown in dir to html    ${INPUT_DIR}    ${RESULT_DIR}    True