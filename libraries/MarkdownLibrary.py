from robot.api.deco import keyword, library
from robot.api.logger import librarylogger as logger
import markdown
import os
from typing import List
from pathlib import Path


@library
class MarkdownLibrary:

    MARKDOWN = markdown.Markdown(
        extensions=['extra', 'codehilite', 'toc', 'smarty', 'sane_lists'],
        extension_configs={
            'codehilite': {
                'noclasses': True,
            }
        }
    )

    @keyword
    def convert_all_markdown_in_dir_to_html(self,
                                            input_dir: str,
                                            output_dir: str = '',
                                            include_subdirectory=False) -> List[str]:
        result = []
        input_path = Path(input_dir)
        list_of_all = input_path.glob('*.md')
        list_of_all_md = [f'{md}' for md in list_of_all]
        if list_of_all_md:
            result.append(self.convert_all_markdown_to_html(list_of_all_md, output_dir))
        if include_subdirectory:
            subdirectories = [sub_dir for sub_dir in input_path.iterdir() if sub_dir.is_dir()]
            for sub_dir in subdirectories:
                sub_folder = f'{sub_dir}'.rsplit('/', 1)[1]
                logger.debug(f'Start looking for markdown files in:\t{sub_folder}')
                result.extend(self.convert_all_markdown_in_dir_to_html(f'{sub_dir}',
                                                                       output_dir=f'{output_dir}/{sub_folder}',
                                                                       include_subdirectory=True))
        return result

    @keyword
    def convert_all_markdown_to_html(self, list_of_all_md: List[str], output_dir: str = '') -> List[str]:
        path_to_all_html = []
        for path_to_md in list_of_all_md:
            path_to_html = self.convert_markdown_to_html(path_to_md, output_dir)
            path_to_all_html.append(path_to_html)
        return path_to_all_html

    @keyword
    def convert_markdown_to_html(self, path_to_md: str, output_dir: str = '') -> str:
        if not output_dir:
            output_dir = os.path.dirname(path_to_md)
        md_basename = os.path.basename(path_to_md)
        md_filename, md_extension = os.path.splitext(md_basename)
        if '.md' != md_extension.lower():
            logger.warn (f'Source file has unexpected file extension. Is the file really markdown?:\t{path_to_md}')
        output_path = Path(f'{output_dir}/{md_filename}.html')
        if not output_path.exists():
            if not os.path.exists(output_dir):
                os.makedirs(output_dir)
            output_path.touch()

        try:
            self.MARKDOWN.convertFile(path_to_md, f'{output_path}')
        except Exception as e:
            os.remove(output_path)
            logger.error(f'Failed to convert {path_to_md} due to:\t{e}')
            raise e
        MarkdownLibrary._replace_md_links(output_path)
        return output_path


    @staticmethod
    def _replace_md_links(path_to_html: Path):
        """
        Links are not correctly converted and still link to Markdown files.

        This simple method replaces all '.md' substring with '.html' which might lead to false results,
        if '.md' is not part of the name of a markdown file.
        """
        # read input file
        with path_to_html.open("rt") as fin:
            # read file contents to string
            data = fin.read()
            # replace all occurrences of the required string
            data = data.replace('.md', '.html')
            # close the input file

        with path_to_html.open('wt') as fout:
            # overrite the input file with the resulting data
            fout.write(data)
