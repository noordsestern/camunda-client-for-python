*** Settings ***
Library    ../../libraries/MarkdownLibrary.py
Library    RPA.FileSystem

*** Variables ***
${INPUT_DIR}    output/docs
${RESULT_DIR}    test_output

*** Tasks ***
Test markdown
    File should not exist    ${RESULT_DIR}/ExternalTaskApi.html
    ${html}    convert markdown to html    ${INPUT_DIR}/ExternalTaskApi.md    ${RESULT_DIR}
    file should exist    ${RESULT_DIR}/ExternalTaskApi.html

*** Keyword ***
File should exist
    [Arguments]    ${file}
    ${file_exists}    does file exist    ${file}
    run keyword if   not ${file_exists}    Fail    ${file} should exist.

File should not exist
    [Arguments]    ${file}
    ${file_exists}    does file exist    ${file}
    Run keyword if    ${file_exists}    Fail    ${file} should not exist.