*** Settings ***
Library    libraries/MarkdownLibrary.py
Library    RPA.FileSystem

*** Variables ***
${INPUT_DIR}    output
${RESULT_DIR}    test_output

*** Tasks ***
Test markdown folder
    directory should not be empty    ${INPUT_DIR}
    directory should be empty    ${RESULT_DIR}
    ${all_htmls}    convert all markdown in dir to html    ${INPUT_DIR}    ${RESULT_DIR}    ${True}
    directory should not be empty    ${RESULT_DIR}

*** Keywords ***
Directory should be empty
    [Arguments]    ${dir}
    ${dir_empty}    is directory empty    ${dir}
    Run keyword if    not ${dir_empty}    Fail    Directory not empty:\t${dir}

Directory should not be empty
    [Arguments]    ${dir}
    ${dir_empty}    is directory empty    ${dir}
    Run keyword if    ${dir_empty}    Fail    Directory is empty:\t${dir}